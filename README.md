# Timezone

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.2.

1. Build your app for production `ng build`
2. Create a firebase project in www.firebase.google.com
3. Install firebase tools `npm install -g firebase-tools`
4. Login using firebase cli `firebase login`
5. Initialize the project `firebase init`
6. Deploy to firebase `firebase deploy`
