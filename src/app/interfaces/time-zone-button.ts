import { TimeZone } from "./time-zone";

export interface TimeZoneButton {
  id: number;
  timeZone: TimeZone
}
