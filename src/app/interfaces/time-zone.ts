export interface TimeZone {
  locale: string;
  timeZone: string;
}
