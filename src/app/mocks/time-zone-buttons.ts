import timeZones from "../enums/time-zones";
import { TimeZoneButton } from "../interfaces/time-zone-button";

export const timeZoneButtons: TimeZoneButton[] = [
  {
    id : 1,
    timeZone : {locale: 'hu-HU', timeZone: timeZones['Europe/Budapest']},
  },
  {
    id : 2,
    timeZone : {locale: 'hu-HU', timeZone: timeZones['America/Bahia']}
  },
  {
    id : 3,
    timeZone : {locale: 'hu-HU', timeZone: timeZones['Mexico/BajaNorte']}
  },
  {
    id : 4,
    timeZone : {locale: 'hu-HU', timeZone: timeZones['Europe/San_Marino']}
  }
];
