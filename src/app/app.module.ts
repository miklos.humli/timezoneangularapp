import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TimeZoneButtonComponent } from './time-zone-button/time-zone-button.component';

@NgModule({
  declarations: [
    AppComponent,
    TimeZoneButtonComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
