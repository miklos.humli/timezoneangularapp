import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from "rxjs";

import TimeZones from "./enums/time-zones";
import { TimeZoneButton } from "./interfaces/time-zone-button";
import { timeZoneButtons } from "./mocks/time-zone-buttons";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit{
  title = 'Timezone app';

  timeZones = TimeZones;

  activatedId$: BehaviorSubject<number> = new BehaviorSubject<number>(1);
  timeZoneButtons: TimeZoneButton[] = timeZoneButtons;
  
  constructor() {
  }

  changedCurrentId(event: number) {
   this.activatedId$.next(event);
  }

  trackByFn(index: number, timeZoneButton: TimeZoneButton): number {
    return timeZoneButton.id;
  }

  ngOnInit(): void {

  }
}
