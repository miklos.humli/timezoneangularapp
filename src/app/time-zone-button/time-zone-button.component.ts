import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TimeZone } from "../interfaces/time-zone";
import { Observable, of, Subject } from "rxjs";

@Component({
  selector: 'app-timezone-button',
  templateUrl: './time-zone-button.component.html',
  styleUrls: ['./time-zone-button.component.scss']
})
export class TimeZoneButtonComponent implements OnInit {

  currentDateTime: string | undefined;

  @Input() timeZone: TimeZone | undefined;
  @Input() id: number | undefined;
  @Input() activetedId$: Subject<number> = new Subject<number>();

  @Output() private changeCurrentId: EventEmitter<number> = new EventEmitter<number>();

  private _disabled$: Observable<boolean> = new Observable<boolean>();

  constructor() {
  }

  get disabled(): Observable<boolean> {
    return this._disabled$;
  }

  setDisabled(status: boolean): void {
    this._disabled$ = of(status);
  }

  getDate(timeZone?: TimeZone): string {
    return new Date().toLocaleString(
      timeZone?.locale,
      <Intl.DateTimeFormatOptions>{timeZone: timeZone?.timeZone}
    );
  }

  ngOnInit(): void {
    this.refreshCurrentDate();
    this.activetedId$.subscribe((res: number) => {
      this.setDisabled(res === this.id);
    });
  }

  refreshCurrentDate(): void {
    if (this.timeZone) this.currentDateTime = this.getDate(this.timeZone);
  }

  changingCurrentId(): void {
    this.changeCurrentId.emit(this.id);

  }
}
